<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
 
$this->title = 'โปรโมชั่นแต่งงาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a('สร้างโปรโมชั่น', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    <div class="clearfix"></div>
<?php Pjax::begin(); ?>    <?=    common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
               
              'header'=>'ชื่อโปรโมชั่น',
              'value'=>'pro_name',
            ],
            [
              'format'=>'raw',
              'header'=>'รูปภาพโปรโมชั่น',
              'value'=>function($model){
                return Html::img(Yii::getAlias('@web').'/promotions/'. $model->image,
                ['width' => '70px']);
              }
            ],
             
            'date',
            [
                'label'=>'ราคา',
                'value'=>'price',
                'format'=>'decimal'
            ],        
            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการ โปรโมชั่น',
                'template' => '{delete}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			
			    return Html::a('<span class="glyphicon glyphicon-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			
                    },
                    'delete' => function ($url, $model) {
			
			    
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			    
			 
                    },
                ],
                'contentOptions' => ['style' => 'width:130px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
