<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
 use yii\helpers\Url;
?>

<div class="row">

     
    <div class="col-md-6">
        <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>


    <?php if(!empty($model->image)): ?>
        <img src='<?= Url::to("@web/promotions/$model->image")?>' class="img img-responsive" style="width:100%;padding:10px;">
    <?php endif; ?>
    <?= $form->field($model, 'pro_name')->label("ตั้งชื่อโปรโมชั่น") ?>     
    <?= $form->field($model, 'price')->label("ราคา") ?>    
    <?= $form->field($model, 'image')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>

</div>
