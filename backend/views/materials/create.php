<?php

use yii\helpers\Html;
 
$this->title = 'เพิ่ม สถานที่';
$this->params['breadcrumbs'][] = ['label' => 'สถานที่', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

   <?= $this->render('_form', [
        'model' => $model,
        'initialPreview'=>[],
        'initialPreviewConfig'=>[]
    ]) ?>

</div>
