<?php

use yii\helpers\Html;
 
$this->title = 'แก้ไข สถานที่: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'สถานที่', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
                'model' => $model,
		         'initialPreview'=>$initialPreview,
		        'initialPreviewConfig'=>$initialPreviewConfig
            ]) ?>

</div>
