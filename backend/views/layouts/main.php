<?php
 
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Wedding',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        [
            'label' => 'โปรโมชั่น',
            'icon' => 'fa fa-share',
            'url' => '#',
            'items' => [
                ['label' => 'โปรโมชั่นแต่งงาน', 'icon' => 'fa fa-file-code-o', 'url' => ['/promotion/index'],],
                ['label' => 'วิธีชำระเงิน', 'icon' => 'fa fa-dashboard', 'url' => ['/payments/index'],],
                ['label' => 'ติดต่อเจ้าของร้าน', 'icon' => 'fa fa-dashboard', 'url' => ['/contacts/index'],],
                ['label' => 'โปรตกแต่งสถานที่', 'icon' => 'fa fa-dashboard', 'url' => ['/materials/index'],],
                 
            ],
        ],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = ['label' => '<i class="fa fa-id-card"></i> การ์ด','url' => ['/card/index']];
        $menuItems[] = ['label' => '<i class="fa fa-heart"></i> ชุดแต่งงาน','url' => ['/wedding/index']];
        $menuItems[] = ['label' => '<i class="fa fa-users"></i> รูปภาพผลงาน','url' => ['/photol/index']];
        $menuItems[] = ['label' => '<i class="fa fa-users"></i> Users','url' => ['/user/index']];
        $menuItems[] = ['label' => '<i class="fa fa-id-card"></i> แต่งหน้าทำผม','url' => ['/make/index']];
         $menuItems[] = ['label' => '<i class="fa fa-id-card"></i> รางานการซื้อขาย','url' => ['/report/index']];
        $menuItems[]=[
            'label' => 'Profile',
            'icon' => '',
            'url' => '#',
            'items' => [
                ['label' => 'เปลี่ยนรหัสผ่าน', 'icon' => 'fa fa-file-code-o', 'url' => ['/user/change_password']],
                ['label' => 'ออกจากระบบ', 'icon' => 'fa fa-dashboard', 'url' => ['/site/logout'],],
                 
            ],
        ];
         
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
        'encodeLabels'=>false,
    ]);
    NavBar::end();
    ?>
<br><br><br>
    <div class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container-fulid">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<style> 
   
   .navbar-inverse {
        background-color: #ff7f7b;
        border-color: #080808;
    }
    .navbar-inverse .navbar-brand {
        color: #ffffff;
    }
    .navbar-inverse .navbar-nav > li > a {
        color: #ffffff;
    }
    .navbar-inverse .btn-link {
        color: #ffffff;
    }
    .container{
        width:95%;
    }
 .wrap {
    min-height: 100%;
    height: auto;
    margin: 0 auto -60px;
    padding: 0 0 60px;
    background: #dcdbdb;
}
.nut{
    background:#ffffff;
    padding:10px;
}
</style>