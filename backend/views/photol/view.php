<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Photoaul */

$this->title = $model->event_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ภาพกิจกรรม'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel" style="width:98%;margin:0 auto; padding:20px;">
<div class="panel-heading">
<a href="<?= Url::to(['index'])?>" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-menu-left"></i> ย้อนกลับ</a>
        <?php if(@Yii::$app->session['user']->user_status == 20): ?>
         <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    <?php endif; ?>
</div>
    <ul class="list-group">
        <li class="list-group-item"><?= $model->event_name?></li>
        <li class="list-group-item"><?= $model->detail?></li>
        <li class="list-group-item"><?= $model->start_date?></li>
    </ul>


    <br>
    <div class="text-center">
         <?= dosamigos\gallery\Gallery::widget(['items' => $model->getThumbnails($model->ref,$model->event_name),
    
     ]);?>
    </div>
</div>

<style>
div#w1 {
    
}
div#w1 a img{
       height: 150px;
    width: 150px;
    /* margin: 5px; */
    /* border: 1px solid #c9c9e0; */
    box-shadow: 0px 0px 1px 0px #212135;
    border-radius: 3px;
}
</style>


<style>
.panel-primary {
    border-color: #f65314;
}
.panel-primary > .panel-heading {
    color: #fff;
    background-color: #F65314;
    border-color: #F65314;
    height: 34px;
    line-height: 10px;
}
</style>
