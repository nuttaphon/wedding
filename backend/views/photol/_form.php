<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
 
use kartik\widgets\FileInput;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\PhotoLibrary */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row "> 
    <div class="col-md-3"></div>     
    <div class="col-md-6">
         <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?=$form->errorSummary($model) ?>

    <?= $form->field($model, 'ref')->hiddenInput(['maxlength' => 50])->label(false); ?>

    <?= $form->field($model, 'event_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'detail')->textarea(['rows' => 3]) ?>

    <div class="form-group field-upload_files">
      <label class="control-label" for="upload_files[]"> ภาพถ่าย </label>
      <div>
        <?= FileInput::widget([
                   'name' => 'upload_ajax[]',
                     'options' => ['multiple' => true,'accept' => 'image/*'], //'accept' => 'image/*' หากต้องเฉพาะ image
                     'pluginOptions' => [
                     'overwriteInitial'=>false,
                     'initialPreviewShowDelete'=>true,
                     'initialPreview'=> $initialPreview,
                     'initialPreviewConfig'=> $initialPreviewConfig,
                      'uploadUrl' => Url::to(['/photol/uploadajax']),
                     'uploadExtraData' => [
                     'ref' => $model->ref,
                     ],
                     'maxFileCount' => 100
                     ]
                   ]);
                   ?>
               </div>
           </div>

           <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'เพิ่ม' : 'แก้ไข', ['class' => ($model->isNewRecord ? 'btn btn-warning' : 'btn btn-warning').' btn-lg btn-block']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
   
</div>
    <style>
      .error-summary {
        background: #F44336;
        padding: 5px;
        color: white;
        border-radius: 3px;
    }
    .file-preview-frame {
    width: 100%;
    /* background: blue; */
    /* float: left; */
}
    </style>