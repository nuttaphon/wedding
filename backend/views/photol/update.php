<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Gallerys */

$this->title = 'แก้ไข Gallerys: ' . $model->event_name;
$this->params['breadcrumbs'][] = ['label' => 'Gallerys', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->event_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
                'model' => $model,
		         'initialPreview'=>$initialPreview,
		        'initialPreviewConfig'=>$initialPreviewConfig
            ]) ?>

</div>
