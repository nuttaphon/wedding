<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput; 
?>

<div class="row">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-4">
        <div class="form-group field-upload_files">
      <label class="control-label" for="upload_files[]"> ภาพถ่าย </label>
      <div>
        <?= FileInput::widget([
                   'name' => 'upload_ajax[]',
                     'options' => ['multiple' => true,'accept' => 'image/*'], //'accept' => 'image/*' หากต้องเฉพาะ image
                     'pluginOptions' => [
                     'overwriteInitial'=>false,
                     'initialPreviewShowDelete'=>true,
                     'initialPreview'=> $initialPreview,
                     'initialPreviewConfig'=> $initialPreviewConfig,
                      'uploadUrl' => Url::to(['/wedding/uploadajax']),
                     'uploadExtraData' => [
                     'ref' => $model->ref,
                     ],
                     'maxFileCount' => 100
                     ]
                   ]);
                   ?>
               </div>
           </div>
    </div>

    <div class="col-md-6">
     <?= $form->field($model, 'we_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'we_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'we_count')->textInput() ?>
         <?= $form->field($model, 'we_price')->textInput(['maxlength' => true]) ?>

     

    <?= $form->field($model, 'ref')->hiddenInput(['maxlength' => 50])->label(false); ?>
<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>
    </div>
    

    <?php ActiveForm::end(); ?>

</div>
