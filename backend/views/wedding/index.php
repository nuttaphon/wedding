<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\WeddingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ชุดแต่งงาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a('<i class="fa fa-plus"></i> เพิ่มชุดแต่งงาน', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    <div class="clearfix"></div>
<?php Pjax::begin(); ?>    <?=    \common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            'we_name',
            'we_description:ntext',
            'we_count',
             
            // 'ref',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการ ชุดแต่งงาน',
                'template' => '{view} {update} {delete}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			
			    return Html::a('<span class="glyphicon glyphicon-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			
                    },
                    'delete' => function ($url, $model) {
			
			    
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			    
			 
                    },
                ],
                'contentOptions' => ['style' => 'width:130px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
