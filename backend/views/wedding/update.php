<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Weddings */

$this->title = 'แก้ไข ชุดแต่งงาน: ' . $model->we_name;
$this->params['breadcrumbs'][] = ['label' => 'ชุดแต่งงาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
                'model' => $model,
		         'initialPreview'=>$initialPreview,
		        'initialPreviewConfig'=>$initialPreviewConfig
            ]) ?>

</div>
