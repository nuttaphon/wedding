<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Weddings */

$this->title = 'เพิ่ม ชุดแต่งงาน';
$this->params['breadcrumbs'][] = ['label' => 'ชุดแต่งงาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

   <?= $this->render('_form', [
        'model' => $model,
        'initialPreview'=>[],
        'initialPreviewConfig'=>[]
    ]) ?>

</div>
