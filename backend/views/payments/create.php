<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Payments */

$this->title = 'เพิ่ม วิธีการชำระเงิน';
$this->params['breadcrumbs'][] = ['label' => 'วิธีการชำระเงิน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
