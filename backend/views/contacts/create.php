<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Contacts */

$this->title = 'เพิ่ม การติดต่อเจ้าของร้าน';
$this->params['breadcrumbs'][] = ['label' => 'การติดต่อเจ้าของร้าน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
