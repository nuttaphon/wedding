<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cards */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Cards',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
                'model' => $model,
		         'initialPreview'=>$initialPreview,
		        'initialPreviewConfig'=>$initialPreviewConfig
            ]) ?>

</div>
