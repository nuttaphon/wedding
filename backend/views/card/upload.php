 <?php 
    echo \kato\DropZone::widget([
       'options' => [
           'name' => 'file', // input name or 'model' and 'attribute'
           'url'=>'upload',
           'maxFilesize' => '10',
           'addRemoveLinks'=>true,
       ],
       'clientEvents' => [
           'complete' => "function(data){console.log(data.name)}",
           'removedfile' => "function(file){
               $.ajax({
                    url:'remove_file',
                    type:'POST',
                    data:{name:file.name},
                    success:function(data){
                        console.log(data);
                    }
                });
            }"
       ],
   ]);
 ?>