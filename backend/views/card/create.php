<?php

use yii\helpers\Html;
 
$this->title = Yii::t('app', 'เพิ่มการ์ดแต่งงาน');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'การ์ดแต่งงาน'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cards-create" style="background: #fff;border-radius: 10px;padding:10px 10px 100px 10px;">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>
   
    <?= $this->render('_form', [
        'model' => $model,
        'initialPreview'=>[],
        'initialPreviewConfig'=>[]
    ]) ?>

</div>
