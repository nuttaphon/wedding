<?php
    $this->title="รายงานการซื้อขาย";
?>
<div class="panel panel-danger">
    <div class="panel-heading"><?= \yii\helpers\Html::encode($this->title)?></div>
    <div class="panel-body">
        <div class="nut">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>รหัสสินค้า</th>
            <th>ชื่อสินค้า</th>
            <th>จำนวน</th>
            <th>ราคา</th>
            <th>วันที่</th>
        </tr>
        
    </thead>
    <tbody>
        <?php foreach($model as $m): ?>
        <tr>
            <td><?= $m->producet_id?></td>
            <td><?= $m->produce_name?></td>
            <td><?= $m->produce_price?></td>
            <td><?= $m->product_count?></td>
            <td><?= $m->date?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
    </div>
</div>