<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'เปลี่ยนรหัสผ่าน');
 
$this->params['breadcrumbs'][] = Yii::t('app', 'เปลี่ยนรหัสผ่าน'); 
?>
<div class="clearfix"></div>
<div class="wrap">
    <div class="col-md-3"></div>
<div class="col-md-6">

    <?php $form = ActiveForm::begin(); ?>
 
    
<div class="panel panel-danger" id="personalForm2" >
    <div class="panel-heading">
        เปลี่ยนรหัสผ่าน
    </div>
    <br>
    <div class="panel-body">
        <div class='form-horizontal'>
             
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-3 control-label"><code>*</code> รหัสผ่านเดิม</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($user,'currentPassword')->passwordInput( )->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-3 control-label"><code>*</code> รหัสผ่านใหม่</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($user,'newPassword')->passwordInput( )->label(false);
                    ?>
                </div>
            </div>
            
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-3 control-label"><code>*</code> ยืนยันรหัสผ่าน</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($user,'newPasswordConfirm')->passwordInput( )->label(false);
                    ?>
                </div>
            </div>
        </div>

    </div>

    <div class="panel-footer">
      <?php 
        echo Html::submitButton('Save',['class'=>'btn btn-primary']);
      ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
    <div class="clearfix"></div><br><br><br><br><br><br><br><br><br><br>