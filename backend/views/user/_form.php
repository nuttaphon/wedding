<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
 
?>
<div class="col-md-3"></div>
<div class="col-md-6">

    <?php $form = ActiveForm::begin(); ?>

    

    
<div class='row'>    
<div class="panel panel-danger" id="personalForm2" >
    <div class="panel-heading">
        ชื่อและข้อมูลการติดต่อ
    </div>
    <br>
    <div class="panel-body">
        <div class='form-horizontal'>
             
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-3 control-label"><code>*</code> ชื่อลอกอิน</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($model,'username')->textInput( )->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-3 control-label"><code>*</code> ชื่อ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($model,'fname')->textInput( )->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-3 control-label"><code>*</code> นามสกุล</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($model,'lname')->textInput( )->label(false);
                    ?>
                </div>
            </div>
            
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-3 control-label"><code>*</code> อีเมล์</label>
                <div class="col-sm-8">
                    <?php 
			echo $form->field($model,'email')->textInput( )->label(false);
//			 
			?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-3 control-label"><code>*</code> เบอร์โทรศัพท์</label>
                <div class="col-sm-8">
                    <?=
                    $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '9999999999',
                    ])->label(false)
                    ?>
                </div>
            </div>
	    
        </div>

    </div>

    <div class="panel-footer">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>