<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\lib\sdii\components\helpers\SDNoty;
use yii\bootstrap\ActiveForm;


$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nut">

    <h1>  <?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

 <p>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'options' => ['class'=>'form-inline text-right']
    ]); ?>
	
    <?php 
    
    
    echo '<div class="form-group">';
    echo '<label for="usersearch-username">ค้นหา</label> ';
    echo Html::activeTextInput($searchModel, 'username', ['style'=>'width: 800px;', 'class'=>'form-control', 'placeholder'=>' ชื่อ-สกุล  ']);
    echo '</div>';
   
      
    ?>
    
    <?php echo Html::submitButton( 'ค้นหา', ['class' => 'btn btn-primary']) ?>
	
    <?php ActiveForm::end(); ?>
    </p>   
<?php Pjax::begin(['id'=>'user-grid-pjax', 'timeout' => 10000]); ?>    
<?=    common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'header' => 'ชื่อ-สกุล',
                'value' => function ($model) {
                    return $model->fname.' '.$model->lname;
                },
                //'headerOptions'=>['style'=>'text-align: center;'],
                //'contentOptions'=>['style'=>'width:190px;'],//text-align: center;
            ],
            [
                'header' => 'Admin?',
                'value' => function ($model) {
                    $role = $model->role;
                    if($role == 20){
                    return '<i class="glyphicon glyphicon-ok"></i>';
                    }
                    return '<i class="glyphicon glyphicon-remove-sign"></i>';
                },
                'format' => 'raw',
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:90px;text-align: center;'],
            ],
            [
              'header'=>'เบอร์โทรศัพท์',
              'value'=>'phone'  
            ],            
            [
                'attribute'=>'created_at',
                'header' => 'วันที่สมัคร',
                'value' => function ($model) {
                    Yii::$app->formatter->locale = 'th';

                   return Yii::$app->formatter->asDate($model->created_at, 'short');
                },
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:90px;text-align: center;'],
            ],  
            

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการผู้ใช้',
                'template' => '{update} {delete}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			
			    return Html::a('<span class="glyphicon glyphicon-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			
                    },
                    'delete' => function ($url, $model) {
			
			    
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			    
			 
                    },
                ],
                'contentOptions' => ['style' => 'width:130px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
