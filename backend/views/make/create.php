<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Makes */

$this->title = 'เพิ่ม แต่งหน้า/ทำผม';
$this->params['breadcrumbs'][] = ['label' => 'แต่งหน้า/ทำผม', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="makes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
