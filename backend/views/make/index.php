<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'แต่งหน้า/ทำผม';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nut">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="pull-right">
        <?= Html::a('เพิ่ม', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    <div class="clearfix"></div>
<?php Pjax::begin(); ?>    <?=    \common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            'name',
            'price',
            [
              'format'=>'raw',
              'header'=>'รูปภาพ',
              'value'=>function($model){
                return Html::img(Yii::getAlias('@web').'/makes/'. $model->image,
                ['width' => '70px']);
              }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการ ',
                'template' => '{delete}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			
			    return Html::a('<span class="glyphicon glyphicon-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			
                    },
                    'delete' => function ($url, $model) {
			
			    
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			    
			 
                    },
                ],
                'contentOptions' => ['style' => 'width:130px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
