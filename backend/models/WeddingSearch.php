<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Weddings;

/**
 * WeddingSearch represents the model behind the search form about `common\models\Weddings`.
 */
class WeddingSearch extends Weddings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'we_count'], 'integer'],
            [['we_name', 'we_description', 'we_price', 'ref'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Weddings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'we_count' => $this->we_count,
        ]);

        $query->andFilterWhere(['like', 'we_name', $this->we_name])
            ->andFilterWhere(['like', 'we_description', $this->we_description])
            ->andFilterWhere(['like', 'we_price', $this->we_price])
            ->andFilterWhere(['like', 'ref', $this->ref]);

        return $dataProvider;
    }
}
