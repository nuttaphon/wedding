<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;        // เรียกใช้ คลาส AccessControl
use common\components\AccessRule;
 
class UserController extends Controller
{
     
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException("ไม่ได้รับอนุญาต");
                },
                'only' => ['index', 'update','delete','change_password'], // กำหนด action ทั้งหมดภายใน Controller นี้
                'ruleConfig' => [
                    'class' => AccessRule::className() // เรียกใช้งาน accessRule (component) ที่เราสร้างขึ้นใหม่
                ],
                'rules' => [
                    [
                        'actions' => ['index','update','delete'],     // กำหนด rules ให้ actionIndex()
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN        // อนุญาตให้ "ผู้ดูแลระบบ" ใช้งานได้
                        ]
                    ],
                    [
                        'actions'=>['change_password'],
                        'allow'=>true,
                        'roles'=>[
                            User::ROLE_ADMIN,
                            User::ROLE_USER
                        ],
                    ]
                ],
            ],
        
        ];
    }  

   
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     
    public function actionChange_password()
    {
      $user = Yii::$app->user->identity; //ตรวจสอบจากการ Login 
      $post = $user->load(Yii::$app->request->post());///ตรวจสอบว่ามีการส่งค่าเป็นแบบ POST มาหรือเปล่า
      
      
          
        if(!empty($post))
        {
            $model = new User();//สร้าง object user เพื่อใช้งาน attribute หรือตัวแปร ใน class User
            $password = Yii::$app->security->generatePasswordHash($_POST["User"]["currentPassword"]);
         
            $sql="UPDATE user SET password='".$password."',password_hash='".$password."' WHERE id = '".$user->id."' ";
            $result=Yii::$app->db->createCommand($sql)->execute();
            if($result){
                \common\lib\message\Messagebox::getSuccess("บันทึกการเปลี่ยนแปลงแล้ว");
            }
            return $this->redirect(['user/index']);
            
        }
         
         
      
      return $this->render('change_password',[
          'user'=>$user
          
      ]);
    }//end Change_password

     
    public function actionCreate()
    {
        $model = new User(); //สร้าง object user เพื่อใช้งาน attribute หรือตัวแปร ใน class User

        if ($model->load(Yii::$app->request->post())) { //ตรวจสอบว่ามีการส่งค่าเป็นแบบ POST มาหรือเปล่า
           if($model->save()) //ถ้าเป็น แบบ POST ก็ให้บันทึกข้อมูล 
            {
                \common\lib\message\Messagebox::getSuccess("บันทึกการเปลี่ยนแปลงแล้ว");//แสดง Message Box
                return $this->redirect(['index']);//กลับไปหน้า User 
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }//end Create 

     
    public function actionUpdate($id)//รับ พารามิเตอร์ id แบบ GET 
    {
        $model = $this->findModel($id);//ตรวจสอบข้อมูลจากฐานข้อมูลผ่าน id ที่เป็น primary 
        $post = \yii::$app->request->post();
        if (!empty($post)) { //ตรวจสอบว่ามีการส่งค่าเป็นแบบ POST มาหรือเปล่า
            $username = $post["User"]["username"];
            $fname  = $post["User"]["fname"];
            $lname = $post["User"]["lname"];
            $phone = $post["User"]["phone"];
            $email = $post["User"]["email"];
            
           // echo $email; exit();
            $sql= \Yii::$app->db->createCommand("UPDATE user SET username=:username, fname=:fname, lname=:lname, email=:email, phone=:phone WHERE id=:id")
            ->bindValue(':username', $username)
            ->bindValue(':fname', $fname)
            ->bindValue(':lname', $lname)
            ->bindValue(':email', $email)
            ->bindValue(':phone', $phone)
            ->bindValue(':id', (int)$id)->execute();        
           
                \common\lib\message\Messagebox::getSuccess('บันทึกการเปลี่ยนแปลงแล้ว');
                return $this->redirect(['index']);
           
            
        } else {//ถ้าไม่ได้ส่งค่าแบบ POST มา ก็ให้ไปเรียก _form มาแสดงเพื่อแก้ไข 
            return $this->render('update', [
                'model' => $model,//ส่งพารามิเตอร์ model ไปเพื่อใช้งาน form 
            ]);
        }
    }//end Update
    
    public function actionDelete($id)//รับ พารามิเตอร์ id แบบ GET 
    {
        if(Yii::$app->user->identity->id == $id){//ถ้าเป็นข้อมูลของตัวเอง 
            \common\lib\message\Messagebox::getDanger("ไม่สามารถลบข้อมูลตังเองได้"); //แสดง Message Box
        }else{//ถ้าไม่ใช่ข้อมูลของตัวเอง
             $this->findModel($id)->delete();//ลบข้อมูล user
        }
       

        return $this->redirect(['index']);//กลับไปหน้า user
    }//end Delete

    
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
