<?php

namespace backend\controllers;

use Yii;
use common\models\Promotion;
use backend\models\PromotionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile; //ใช้สำหรัง upload image
 
class PromotionController extends Controller
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

     
    
    public function actionIndex()
    {
        $searchModel = new PromotionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    public function actionCreate()
    {
       
        $model = new Promotion();


        $uploadPath = Yii::getAlias('@web') .'/promotions/';

        
        if ($model->load(Yii::$app->request->post())) {
           //$uploadPath = Yii::getAlias('@web') .'/package/';
             
          if(!empty($_FILES))
           {
            
             //เก็บไฟล์ภาพไว็ยังไม่อัปโหลด  
             $model->image = UploadedFile::getInstance($model, 'image');

             //เปลี่ยนชื่อไฟล์ซักหน่อย ให้ดูดี
             $newFileName = \Yii::$app->security->generateRandomString().'.'.$model->image->extension;

            //ดึงข้อมูล รูปภาพจาก ฐานข้อมูลมาตรวจสอบ ดูนะ ว่าซ้ำกันหรือ ไม่ ถ้าซ้ำ ให้ random ใหม่ ซะเด้อ
            $cimg = Promotion::find()->all();
            foreach($cimg as $c)
            {
                if($newFileName == $c->image)
                {
                   $newFileName = \Yii::$app->security->generateRandomString().'.'.$model->image->extension; 
                }
            }
            $model->image->saveAs('promotions/' . $newFileName);
            $model->image = $newFileName;
            if($model->save())
            {
                \common\lib\message\Messagebox::getSuccess("บันทึกเรียบร้อย");
                return $this->redirect(['index']);
            }
               
           }


        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        } 
    }

     
    public function actionUpdate($id)
    {
        $model = Promotion::findOne($id);
          $fimg = $model->image; 
        $uploadPath = Yii::getAlias('@web') .'/promotions/';
        
         
      
        if ($model->load(Yii::$app->request->post())) {
        
           foreach(@$_FILES['Promotions']['name'] as $f)
           {
               $file = $f;
           }
           if(empty($file))
           {
              $model->image = $fimg; //เมื่อไม่ได้ เลือกไฟล์ที่จะ อัปโหลด 
            
           }else{
                 @unlink('promotions/'.$fimg);
                 //เก็บไฟล์ภาพไว็ยังไม่อัปโหลด  
                 $model->image = UploadedFile::getInstance($model, 'img');

             //เปลี่ยนชื่อไฟล์ซักหน่อย ให้ดูดี
                 $newFileName = \Yii::$app->security->generateRandomString().'.'.$model->image->extension;

                //ดึงข้อมูล รูปภาพจาก ฐานข้อมูลมาตรวจสอบ ดูนะ ว่าซ้ำกันหรือ ไม่ ถ้าซ้ำ ให้ random ใหม่ ซะเด้อ
                $cimg = Promotion::find()->all();
                foreach($cimg as $c)
                {
                    if($newFileName == $c->image)
                    {
                       $newFileName = \Yii::$app->security->generateRandomString().'.'.$model->image->extension; 
                    }
                }
                $model->image->saveAs('promotions/' . $newFileName);
                $model->image = $newFileName;
                
           }
         

           if($model->save())
                {
               \common\lib\message\Messagebox::getSuccess("บันทึกเรียบร้อย");
                    return $this->redirect(['index']);
                }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    
     
    public function actionDelete($id)
    {
        $model = Promotion::findOne($id);
        @unlink('promotions/'.$model->image);
        $model->delete();

        return $this->redirect(['index']);
    }

    
    protected function findModel($id)
    {
        if (($model = Promotion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
