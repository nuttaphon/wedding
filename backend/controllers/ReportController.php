<?php

 
namespace backend\controllers;

 use yii;
 
class ReportController extends \yii\web\Controller{
    public function actionIndex(){
        $model= \common\models\Reports::find()->orderBy(['id'=>SORT_DESC])->all();
        return $this->render('index',[
            'model'=>$model
        ]);
        //print_r($model);
    }
}
