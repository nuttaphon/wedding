<?php

namespace backend\controllers;

use Yii;
use common\models\Makes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile; //ใช้สำหรัง upload image

class MakeController extends Controller
{
    
   
     
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Makes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

   public function actionCreate()
    {
       
        $model = new Makes();


        $uploadPath = Yii::getAlias('@web') .'/makes/';

        
        if ($model->load(Yii::$app->request->post())) {
           //$uploadPath = Yii::getAlias('@web') .'/package/';
             
          if(!empty($_FILES))
           {
            
             //เก็บไฟล์ภาพไว็ยังไม่อัปโหลด  
             $model->image = UploadedFile::getInstance($model, 'image');

             //เปลี่ยนชื่อไฟล์ซักหน่อย ให้ดูดี
             $newFileName = \Yii::$app->security->generateRandomString().'.'.$model->image->extension;

            //ดึงข้อมูล รูปภาพจาก ฐานข้อมูลมาตรวจสอบ ดูนะ ว่าซ้ำกันหรือ ไม่ ถ้าซ้ำ ให้ random ใหม่ ซะเด้อ
            $cimg = Makes::find()->all();
            foreach($cimg as $c)
            {
                if($newFileName == $c->image)
                {
                   $newFileName = \Yii::$app->security->generateRandomString().'.'.$model->image->extension; 
                }
            }
            $model->image->saveAs('makes/' . $newFileName);
            $model->image = $newFileName;
            if($model->save())
            {
                \common\lib\message\Messagebox::getSuccess("บันทึกเรียบร้อย");
                return $this->redirect(['index']);
            }
               
           }


        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        } 
    }
 

   
    public function actionDelete($id)
    {
        $model = Makes::findOne($id);
        @unlink('makes/'.$model->image);
        $model->delete();

        return $this->redirect(['index']);
    }

    
    protected function findModel($id)
    {
        if (($model = Makes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
