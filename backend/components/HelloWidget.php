<?php

namespace backend\components;
use yii\helpers\Html;
use yii\base\Widget;
class HelloWidget extends \yii\base\Widget{
     
    public function init(){
        parent::init();
        ob_start();
    }
    public function run(){
       $content = ob_get_clean();
       return Html::encode($content);
    }
} 