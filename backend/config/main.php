<?php

# Virtual Host
use \yii\web\Request;
$baseUrl = str_replace('/web', '', (new Request)->getBaseUrl());


$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'users' => [
            'class' => 'backend\modules\users\Users',
        ],
         'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'components' => [
//        'view' => [
//             'theme' => [
//                 'pathMap' => [
//                     '@backend/views' => '@themes/adminlte'
//                 ],
//             ],
//        ],
        'image' => [   
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD',  //GD or Imagick
        ],//////////////////////UPLOAD IMG
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => $baseUrl,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
       # Virtual Host
        'urlManager' => [   
            
            'showScriptName' => false,   // Disable index.php
            'enablePrettyUrl' => true,   // Disable r= routes
            'enableStrictParsing' => true,
            
            'rules' => array(
                
                # Basic
                '' => 'site/index',
                
                # Yii2-User Extension
                'manager' => 'user/admin/index', //  หน้าจอจัดการข้อมูล user
                'user/<controller:\w+>/<action:\w+>' => 'user/<controller>/<action>',
                
                # Yii2-Admin Extension
                'admin' => 'admin/assignment',
                'admin/<controller:\w+>/<action:\w+>' => 'admin/<controller>/<action>',
                
                # Other
                '<controller:\w+>' => '<controller>/index',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>' => '<controller>/<action>',
                
                # Modules
                '<module:\w+>' => '<module>/default/index',
                '<module:\w+>/<controller:\w+>' => '<module>/<controller>/index',
                '<module:\w+>/<controller:\w+>/<action>' => '<module>/<controller>/<action>',
                
            ),
            
        ], 
    ],
    'params' => $params,
];
