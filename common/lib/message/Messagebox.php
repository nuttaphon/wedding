<?php

namespace common\lib\message;

use Yii;
class Messagebox {
  
    public function getSuccess($msg){
        return Yii::$app->session->setFlash("success",$msg);
    }
    
    public function getInfo($msg){
        return Yii::$app->session->setFlash("info",$msg);
    }
    
    public function getPrimary($msg){
        return Yii::$app->session->setFlash("primary",$msg);
    }
    public function getDanger($msg){
        return Yii::$app->session->setFlash("danger",$msg);
    }
    public function getWarning($msg){
        return Yii::$app->session->setFlash("warning",$msg);
    }
    
}
