<?php
namespace common\lib\damasac\widgets;

use common\lib\damasac\assets\datetime\DMSDatetimeAsset;
use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: Balz PC
 * Date: 31-Aug-15
 * Time: 12:52
 */
class DMSDatetimeWidget extends InputWidget
{
    public $id;

    public $options = ['class'=>'form-control'];

    public $clientOptions = ['language'=>'th-th'];

    public $pluginOptions;

    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        DMSDatetimeAsset::register($this->getView());
    }
    public function run(){
        $this->javascriptClient();
        echo Html::beginTag('div',['class'=>'input-group','style'=>'width:100%']);

        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute,$this->options);
        } else {
            echo Html::textInput($this->name, $this->value,$this->options);
        }
        echo Html::beginTag('div',['class'=>'input-group-addon','style'=>'width:50px;']);
        echo Html::beginTag('i',['class'=>'fa fa-calendar']);
        echo Html::endTag('i');
        echo Html::endTag('div');
        echo Html::endTag('div');
    }
    public function setClientOptions(){
        $this->pluginOptions = Json::htmlEncode($this->clientOptions);
    }
    public function javascriptClient(){
        $this->setClientOptions();
        $this->getView()->registerJs("
            jQuery.datetimepicker.setLocale('th');
            $('#".$this->options["id"]."').datetimepicker({
                isBE: true,
                autoConversionField: true,
                lang:'ru',
                format:'Y-m-d H:i:s'
            }).inputmask('9999-99-99 99:99:99');;

        ");
    }
}