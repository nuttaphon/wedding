<?php
namespace common\lib\damasac\widgets;

use common\lib\damasac\assets\date\DMSDateAsset;
use Yii;
use yii\helpers\Json;
use yii\widgets\InputWidget;
use yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: Balz PC
 * Date: 31-Aug-15
 * Time: 12:52
 */
class DMSDateWidget extends InputWidget
{
    public $id;

    public $options = ['class'=>'form-control','placeholder'=>'dd/mm/yyyy'];
//'language'=>'th-th'
    public $clientOptions = ['language'=>'th-th','format'=>'dd/mm/yyyy'];

    public $pluginOptions;

    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        DMSDateAsset::register($this->getView());
    }
    public function run(){
        $this->javascriptClient();

        echo Html::beginTag('div',['class'=>'input-group','style'=>'width:100%']);
        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->value, $this->options);
        }
        echo Html::beginTag('div',['class'=>'input-group-addon','style'=>'width:50px;']);
        echo Html::beginTag('i',['class'=>'fa fa-calendar']);
        echo Html::endTag('i');
        echo Html::endTag('div');
        echo Html::endTag('div');
    }
    public function setClientOptions(){
        $this->pluginOptions = Json::htmlEncode($this->clientOptions);
    }
    public function javascriptClient(){
        $this->setClientOptions();
        $this->getView()->registerJs("
            $('#".$this->options["id"]."').datepicker(
                ".$this->pluginOptions."
            ).inputmask('99/99/9999');

        ");
    }
}