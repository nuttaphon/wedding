<?php
namespace common\lib\damasac\assets\checkbox;

use yii\web\AssetBundle;

class DMSCheckboxAsset extends AssetBundle
{
    public $sourcePath='@lib/damasac/assets/checkbox';

    public $css=[
        'css/regular_checkbox.css'
    ];

    public $js=[

    ];

    public $depends=[
        'yii\jui\JuiAsset',
    ];
}
