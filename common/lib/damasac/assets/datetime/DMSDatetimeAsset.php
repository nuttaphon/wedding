<?php
namespace common\lib\damasac\assets\datetime;

use yii\web\AssetBundle;

class DMSDatetimeAsset extends AssetBundle
{
    public $sourcePath='@lib/damasac/assets/datetime';

    public $css=[
        'css/jquery.datetimepicker.css'
    ];

    public $js=[
        'js/jquery.datetimepicker.full.min.js',
        'js/jquery.datetimepicker.full.js',
        'js/inputmask.js',
        'js/jquery.inputmask.js'
    ];

    public $depends=[
        'yii\jui\JuiAsset',
    ];
}
