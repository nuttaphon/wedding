<?php
namespace common\lib\damasac\assets\time;

use yii\web\AssetBundle;

class DMSTimeAsset extends AssetBundle
{
    public $sourcePath='@lib/damasac/assets/time';

    public $css=[
        'css/bootstrap-timepicker.css'
    ];

    public $js=[
        'js/bootstrap-timepicker.js'
    ];

    public $depends=[
        'yii\jui\JuiAsset',
    ];
}
