<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reports".
 *
 * @property integer $id
 * @property integer $producet_id
 * @property string $produce_name
 * @property integer $produce_price
 * @property integer $user_id
 * @property integer $product_count
 * @property string $date
 */
class Reports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producet_id', 'produce_name', 'produce_price', 'user_id', 'product_count', 'date'], 'required'],
            [['producet_id', 'produce_price', 'user_id', 'product_count'], 'integer'],
            [['date'], 'safe'],
            [['produce_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producet_id' => 'Producet ID',
            'produce_name' => 'Produce Name',
            'produce_price' => 'Produce Price',
            'user_id' => 'User ID',
            'product_count' => 'Product Count',
            'date' => 'Date',
        ];
    }

    public function getReport($producet_id, $produce_name, $produce_price, $user_id, $product_count, $date){
        $model = new Reports();
        $model->producet_id = $producet_id;
        $model->produce_name = $produce_name;
        $model->produce_price = $produce_price;
        $model->user_id = $user_id;
        $model->product_count = $product_count;
        $model->date = $date;
        $model->save();

    }
}
