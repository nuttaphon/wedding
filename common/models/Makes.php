<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "makes".
 *
 * @property integer $id
 * @property string $name
 * @property integer $price
 * @property string $image
 */
class Makes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'makes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['price'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ชื่อรายการ',
            'price' => 'ราคา',
            'image' => 'รูปภาพ',
        ];
    }
}
