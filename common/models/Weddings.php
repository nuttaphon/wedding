<?php

namespace common\models;

use Yii;
use yii\helpers\Url; 
class Weddings extends \yii\db\ActiveRecord
{
    const UPLOAD_FOLDER='weddings';
    public static function tableName()
    {
        return 'weddings';
    }

     
    public function rules()
    {
        return [
            [['we_name', 'we_description', 'we_count', 'we_price', 'ref'], 'required'],
            [['we_description'], 'string'],
            [['we_count'], 'integer'],
            [['we_name', 'we_price', 'ref'], 'string', 'max' => 255],
        ];
    }

     
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'we_name' => 'ชื่อชุดแต่งงาน',
            'we_description' => 'รายละเอียด',
            'we_count' => 'จำนวน',
            'we_price' => 'ราคา',
            'ref' => 'Ref',
        ];
    }
    
    public static function getUploadPath(){
        //return Yii::$app->basePath . '/../storage/web/'.self::UPLOAD_FOLDER.'/';
        return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
       // return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
        // $uploadPath = Yii::$app->basePath . '/../storage/web/';  webroot
    }

    public static function getUploadUrl(){
        return Url::base(true).'/'.self::UPLOAD_FOLDER.'/';
    }

    public function getThumbnails($ref,$event_name){
         $uploadFiles   = Uploads::find()->where(['ref'=>$ref])->all();
         $preview = [];
        foreach ($uploadFiles as $file) {
            $preview[] = [
                'url'=>self::getUploadUrl(true).$ref.'/'.$file->real_filename,
                'src'=>self::getUploadUrl(true).$ref.'/thumbnail/'.$file->real_filename,
                'options' => ['title' => $event_name],
                
            ];
        }
        return $preview;
    }
    
    
}
