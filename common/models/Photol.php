<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
 
class Photol extends \yii\db\ActiveRecord
{
    const UPLOAD_FOLDER='photols';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo_l';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['detail'], 'string'],
            [['start_date'], 'safe'],
            [['ref'], 'string', 'max' => 50],
            [['event_name'], 'string', 'max' => 255],
            [['ref'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ref' => 'เลข fk กับ upload ใช้กับ upload ajax',
            'event_name' => 'ชื่องาน',
            'detail' => 'รายละเอียด',
            'start_date' => 'วันที่',
        ];
    }

    public static function getUploadPath(){
        //return Yii::$app->basePath . '/../storage/web/'.self::UPLOAD_FOLDER.'/';
        return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
       // return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
        // $uploadPath = Yii::$app->basePath . '/../storage/web/';  webroot
    }

    public static function getUploadUrl(){
        return Url::base(true).'/'.self::UPLOAD_FOLDER.'/';
    }

    public function getThumbnails($ref,$event_name){
         $uploadFiles   = Uploads::find()->where(['ref'=>$ref])->all();
         $preview = [];
        foreach ($uploadFiles as $file) {
            $preview[] = [
                'url'=>self::getUploadUrl(true).$ref.'/'.$file->real_filename,
                'src'=>self::getUploadUrl(true).$ref.'/thumbnail/'.$file->real_filename,
                'options' => ['title' => $event_name],
                
            ];
        }
        return $preview;
    }
}
