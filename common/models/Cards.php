<?php

namespace common\models;

use Yii;
 use yii\helpers\Url;
class Cards extends \yii\db\ActiveRecord
{
     const UPLOAD_FOLDER='cards';
    public static function tableName()
    {
        return 'cards';
    }

     
    public function rules()
    {
        return [
            [['card_amount','price','card_name'], 'required'],
            [['card_amount'], 'integer'],
            [['created_st', 'updated_st'], 'safe'],
            [['card_name'], 'string', 'max' => 255],
            [['ref'], 'string', 'max' => 50],
            [['ref'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'card_name' => 'ชื่อการ์ด',
             
            'card_amount' => 'จำนวน',
            'created_st' => 'วันที่เพิ่ม',
            'updated_st' => 'วันที่อัปเดรท',
        ];
    }
    
    
    public static function getUploadPath(){
        //return Yii::$app->basePath . '/../storage/web/'.self::UPLOAD_FOLDER.'/';
        return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
       // return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
        // $uploadPath = Yii::$app->basePath . '/../storage/web/';  webroot
    }

    public static function getUploadUrl(){
        return Url::base(true).'/'.self::UPLOAD_FOLDER.'/';
    }

    public function getThumbnails($ref,$event_name){
         $uploadFiles   = Uploads::find()->where(['ref'=>$ref])->all();
         $preview = [];
        foreach ($uploadFiles as $file) {
            $preview[] = [
                'url'=>self::getUploadUrl(true).$ref.'/'.$file->real_filename,
                'src'=>self::getUploadUrl(true).$ref.'/thumbnail/'.$file->real_filename,
                'options' => ['title' => $event_name],
                
            ];
        }
        return $preview;
    }
}
