<?php

namespace common\models;

use Yii;
 
class Uploads extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_date'], 'safe'],
            //[['type'], 'integer'],
            //[['ref'], 'string', 'max' => 50],
            //[['file_name', 'real_filename'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'upload_id' => 'Upload ID',
            'ref' => 'Ref',
            'file_name' => 'File Name',
            'real_filename' => 'Real Filename',
            'create_date' => 'Create Date',
            'type' => 'Type',
        ];
    }
    
    
}
