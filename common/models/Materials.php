<?php

namespace common\models;

use Yii;

 use yii\helpers\Url; 
class Materials extends \yii\db\ActiveRecord
{
    const UPLOAD_FOLDER='material';
    public static function tableName()
    {
        return 'materials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ma_name', 'ma_description', 'ma_price', 'ref'], 'required'],
            [['ma_description'], 'string'],
            [['ma_name', 'ma_price', 'ref'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ma_name' => 'ชื่อสถานที่',
            'ma_description' => 'รายละเอียด',
            'ma_price' => 'ราคา',
            'ref' => 'Ref',
        ];
    }
    
    
    public static function getUploadPath(){
        //return Yii::$app->basePath . '/../storage/web/'.self::UPLOAD_FOLDER.'/';
        return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
       // return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
        // $uploadPath = Yii::$app->basePath . '/../storage/web/';  webroot
    }

    public static function getUploadUrl(){
        return Url::base(true).'/'.self::UPLOAD_FOLDER.'/';
    }

    public function getThumbnails($ref,$event_name){
         $uploadFiles   = Uploads::find()->where(['ref'=>$ref])->all();
         $preview = [];
        foreach ($uploadFiles as $file) {
            $preview[] = [
                'url'=>self::getUploadUrl(true).$ref.'/'.$file->real_filename,
                'src'=>self::getUploadUrl(true).$ref.'/thumbnail/'.$file->real_filename,
                'options' => ['title' => $event_name],
                
            ];
        }
        return $preview;
    }
}
