<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $pro_id
 * @property string $pro_name
 * @property integer $user_id
 * @property integer $count
 * @property integer $total
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pro_id', 'pro_name', 'user_id', 'count', 'total'], 'required'],
            [['pro_id', 'user_id', 'count', 'total'], 'integer'],
            [['pro_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pro_id' => 'Pro ID',
            'pro_name' => 'Pro Name',
            'user_id' => 'User ID',
            'count' => 'Count',
            'total' => 'Total',
        ];
    }
}
