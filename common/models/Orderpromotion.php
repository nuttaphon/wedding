<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_promotion".
 *
 * @property integer $id
 * @property integer $pro_id
 * @property integer $user_id
 * @property integer $count
 * @property integer $total
 */
class Orderpromotion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_promotion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pro_id', 'user_id', 'total'], 'required'],
            [['pro_id', 'user_id', 'count', 'total'], 'integer'],
            [['count'],'required','message'=>'จำนวน ต้องเป็นตัวเลขเท่านั้น'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pro_id' => 'Pro ID',
            'user_id' => 'User ID',
            'count' => 'Count',
            'total' => 'Total',
        ];
    }
    public function getPromotion()
    {
        return $this->hasOne(Promotion::className(),['id'=>'pro_id']);
    }
    public function getWedding()
    {
        return $this->hasOne(Weddings::className(),['id'=>'pro_id']);
    }
    public function getCard()
    {
        return $this->hasOne(Cards::className(),['id'=>'pro_id']);
    }
}
