<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title="รายการ ที่คุณเลือก"; 
?>
<br>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="panel panel-danger">
            <div class="panel-heading"><?= Html::encode($this->title)?> <?= $marks->name; ?></div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                    <img class="img img-responsive" src="<?= Url::to('../backend/web/makes/' . $marks->image) ?>" alt="...">
                <br>
                    

                <div class="form-group">
                    <?= Html::submitButton('ยืนยัน',['class'=>'btn btn-primary'])?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>