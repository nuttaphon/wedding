<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title="ตระกร้าสินค้า";
    //print_r($model);
?><br>
<div style="       background: rgb(231, 147, 180);
     padding: 5px;
     color: white;
     font-size: 14pt;margin-bottom: 3px;"><i class="glyphicon glyphicon-shopping-cart"></i> <?= $this->title; ?></div>



<div class="table-responsive">
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>รายการสินค้า</th>
                <th>จำนวน</th>
                <th>ราคา</th>
                <th style="width:20px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model as $o): ?>
                <tr>
                    <td><?= $o->pro_name; ?></td>
                    <td><?= $o->count; ?></td>
                    <td><?= number_format($o->total, 2); ?>
                    <td>

                        <a href="<?= Url::to(['delete', 'id' => $o->id]) ?>" class="btn btn-danger">ลบ</a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td>ผลรวม</td>
                <td><?= number_format($sums, 2); ?></td>
                <td>บาท</td>
            </tr>
        </tfoot>
    </table>
    <?php if(!empty($model)): ?>
    <div style="padding:20px;margin-bottom:20px;">
        <a href="<?= Url::to(['confirms', 'id' => Yii::$app->user->identity->id]) ?>" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-ok"></i> ยืนยันการสั่งซื้อ</a>
    </div>
    <?php endif; ?>
</div>
 
<input type="button" onclick="printdiv('div_print');" value="พิมพ์" class="btn btn-danger"/>

<div id="div_print">
<div style="       background: rgb(231, 147, 180);
     padding: 5px;
     color: white;
     font-size: 14pt;margin-bottom: 3px;"><i class="glyphicon glyphicon-shopping-cart"></i> รายการสั่งซื้อสินค้า</div>



<?php 
$user_id2 = Yii::$app->user->identity->id;
$order2 = \common\models\Orders::find()->where(['user_id'=>$user_id2])->all();
$sum2 = Yii::$app->db->createCommand("select sum(total) as s from orders where user_id ='". $user_id2."' ")->queryOne();
$sums2= $sum2['s'];
 
?>     


<div class="table-responsive" >
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>รายการสินค้า</th>
                <th>จำนวน</th>
                <th>ราคา</th>
                <td>
                    
                </td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($order2 as $ob): ?>
                <tr>
                    <td><?= $ob->pro_name; ?></td>
                    <td><?= $ob->count; ?></td>
                    <td><?= number_format($ob->total, 2); ?>
                         <td></td>
                    
                </tr>
            <?php endforeach; ?>

        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td>ผลรวม</td>
               
                <td><?= number_format($sums2, 2); ?></td>
                <td>บาท</td>
            </tr>
        </tfoot>
    </table>
    
</div>
</div>
<script language="javascript">
function printdiv(printpage)
{
var headstr = "<html><body>";
var footstr = "</body>";
var newstr = document.all.item(printpage).innerHTML;
var oldstr = document.body.innerHTML;
document.body.innerHTML = headstr+newstr+footstr;
window.print();
document.body.innerHTML = oldstr;
return false;
}
</script>
<style>
    @media print {
  body * {
    visibility: hidden;
    margin:5px;
  
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
  
}
</style>