<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'หน้าแรก';
?>
<hr>
<div style="       background: rgb(231, 147, 180);
     padding: 5px;
     color: white;
     font-size: 14pt;margin-bottom: 3px;">ผลงานร้าน</div>

 <div class="row">
<?php foreach ($photol as $p): ?>
    <?php
    //ผลงานของร้าน
    $u = \common\models\Uploads::find()->where(['ref' => $p->ref])->one();
    $img = $u->ref . '/' . $u->real_filename;
    ?>
   
        <div class="col-md-4">
            <a href="<?= Url::to(['photoview','id'=>$p->id])?>" class="thumbnail">
                <img class="img img-responsive" src="<?= Url::to('backend/web/photols/' . $img) ?>" style="height:200px;">
            </a>
        </div>
<?php endforeach; ?>
     <div class="clearfix"></div>
     <?= LinkPager::widget(['pagination'=>$pagination])?>
  </div>       

 <div style="       background: rgb(231, 147, 180);
     padding: 5px;
     color: white;
     font-size: 14pt;margin-bottom: 3px;">วิธีติดต่อเจ้าของร้าน</div>

<div class="row">
<?php
        //ติดต่อเจ้าของร้าน
        $content = common\models\Contacts::find()->all();
    ?>
<?php foreach ($content as $c): ?>
     <div class="alert alert-default" role="alert"><?= $c->name; ?></div>
<?php endforeach; ?>
</div> 

 <div style="       background: rgb(231, 147, 180);
     padding: 5px;
     color: white;
     font-size: 14pt;margin-bottom: 3px;">วิธีการชำระเงิน</div>

<div class="row">
<?php
        //ติดต่อเจ้าของร้าน
        $paments = common\models\Payments::find()->all();
    ?>
<?php foreach ($paments as $pa): ?>
     <div class="alert alert-default" role="alert"><?= $pa->name; ?></div>
<?php endforeach; ?>
</div>  