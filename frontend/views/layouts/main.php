<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
    </head>
    <body>
<?php $this->beginBody() ?>

        <style>
            /* Note: Try to remove the following lines to see the effect of CSS positioning */
            .affix {
                top: 0;
                width: 100%;
            }
            .navbar {
                border-radius: 0px;
            }
            .affix + .container-fluid {
                padding-top: 70px;
            }
            .navbar-inverse {
                background-color: #ffffff;
                border-color: #ffffff;
                z-index: 100;
            }
            .navbar-inverse .navbar-nav > li > a {
                color: #ffffff;
            }
            .navbar-inverse .navbar-brand {
                color: #ffffff;
            }
            .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
                color: #fff;
                background-color: #d253a8;
            }
            .navbar-inverse .btn-link {
                color: #ffffff;
            }
            .wrap {
                min-height: 100%;
                height: auto;
                margin: 0 auto -60px;
                padding: 0 0 60px;
                background: #ดfffff;
            }
            .wrap > .container {
                padding: 19px 15px 20px;
            }
            .navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {
                border-color: #101010;
                background: #e793b4;
            }
            .row {
    margin-right: 0px;
    margin-left: 0px;
}
        </style>
        <div class="container-fluid" style="background-color:#fff;color:#fff;    height: 270px;text-align:center;">
            <img src="<?= Url::to('@web/img/bg.jpg') ?>" style="    width: 1140px;height: 270px;">
        </div>
        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => '',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse',
                    'data-spy' => 'affix',
                    'data-offset-top' => '197',
                ],
            ]);
            $menuItems = [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label' => 'โปรโมชั่นแต่งงาน', 'url' => ['/promotion/index']],
                    ['label' => 'ชุดแต่งงาน', 'url' => ['/wedding/index']],
                    ['label' => 'การ์ดแต่งงาน', 'url' => ['/card/index']],
                    ['label' => 'แต่งหน้าทำผม', 'url' => ['/mark/index']],
                    ['label' => 'โปรโมชั่นตกแต่งสถานที่', 'url' => ['/material/index']],
                    ['label' => 'ตะกร้าสินค้า', 'url' => ['/cart/index']],
                
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

            <div class="" style="    width: 1138px;
    margin: 0 auto;
    margin-top: -20px;
    background: rgba(222, 222, 222, 0.35);
    border-right: 1px solid #e9e9e9;
    border-left: 1px solid #e9e9e9;">
                 
                <?= Alert::widget() ?>
<?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
