<?php
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    $this->title="ชุดแต่งงาน";
    //หน้าที่แสดง การ์ดให้เลือก
?>
<br>
<div style="       background: rgb(231, 147, 180);
     padding: 5px;
     color: white;
     font-size: 14pt;margin-bottom: 3px;"><i class="glyphicon glyphicon-check"></i> <?= $this->title; ?></div>

 <div class="row">
<?php foreach ($card as $p): ?>
    <?php
    //ผลงานของร้าน
    $u = \common\models\Uploads::find()->where(['ref' => $p->ref])->one();
    $img = $u->ref . '/' . $u->real_filename;
    ?>
   
        <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img class="img img-responsive" src="<?= Url::to('backend/web/cards/' . $img) ?>" style="height:200px;">
                <div class="caption">
                    
                    <p><a href="<?= Url::to(['order','id'=>$p->id])?>" class="btn btn-primary"><i class="glyphicon glyphicon-shopping-cart
"></i> เลือก</a></p>
                </div>
              </div>
            </div>
<?php endforeach; ?>
     <div class="clearfix"></div>
     <?= LinkPager::widget(['pagination'=>$pagination])?>
  </div> 