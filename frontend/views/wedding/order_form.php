<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title="ชุดแต่งงาน ที่คุณเลือก ".$wedding->we_name. " ราคา: ".$wedding->we_price." บาท";  
 $images = $img->ref . '/' . $img->real_filename;
?>
<br>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="panel panel-danger">
            <div class="panel-heading"><?= Html::encode($this->title)?></div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                    
                    <img class="img img-responsive" src="<?= Url::to('../backend/web/weddings/' . $images) ?>" alt="...">
                
                    <?=
                    $form->field($order, 'count')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '9',
                    ])->label("จำนวน")
                    ?>

                <div class="form-group">
                    <?= Html::submitButton('ยืนยัน',['class'=>'btn btn-primary'])?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>