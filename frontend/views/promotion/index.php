<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
 
$this->title = 'โปรโมชั่น';
$this->params['breadcrumbs'][] = $this->title;
?><br>
<div style="       background: rgb(231, 147, 180);
     padding: 5px;
     color: white;
     font-size: 14pt;margin-bottom: 3px;"><i class="glyphicon glyphicon-check"></i> <?= $this->title; ?></div>
 
 
        <div class="row">
            <?php foreach ($promotion as $p): ?>
            <?php
            //โปรโมชั่น
            ?>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="<?= Url::to('backend/web/promotions/' . $p->image) ?>" alt="...">
                <div class="caption">
                    
                    <p><a href="<?= Url::to(['order','id'=>$p->id])?>" class="btn btn-primary"><i class="glyphicon glyphicon-shopping-cart
"></i> เลือก</a></p>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
     <div class="clearfix"></div>
     <?= LinkPager::widget(['pagination'=>$pagination])?>  
      </div>
 