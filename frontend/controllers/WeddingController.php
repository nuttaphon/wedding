<?php
 
namespace frontend\controllers;
use Yii;

use common\models\User;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;        // เรียกใช้ คลาส AccessControl
use common\components\AccessRule;
use yii\helpers\Url; 
class WeddingController extends \yii\web\Controller{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException("ไม่ได้รับอนุญาตกรุณา Login ");
                },
                'only' => ['order'], // กำหนด action ทั้งหมดภายใน Controller นี้
                'ruleConfig' => [
                    'class' => AccessRule::className() // เรียกใช้งาน accessRule (component) ที่เราสร้างขึ้นใหม่
                ],
                'rules' => [
                    [
                        'actions' => ['order'],     // กำหนด rules ให้ actionIndex()
                        'allow' => true,
                        'roles' => [
                             User::ROLE_USER,        // อนุญาตให้ "user ที่ login" 
                            User::ROLE_ADMIN        // อนุญาตให้ "ผู้ดูแลระบบ" ใช้งานได้
                        ],
                    ],
                     
                ],
            ],
        
        ];
    }//กำหนดให้ login ก่อนถึงจะเข้า method ได้ 
    public function actionIndex(){
        
        $query = \common\models\Weddings::find();
        $pagination = new \yii\data\Pagination([
            'defaultPageSize'=>10,
            'totalCount'=>$query->count(),
        ]);
        $wedding = $query->orderBy(['id'=>SORT_DESC])
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
        
        
        return $this->render('index',[
            'wedding'=>$wedding,
            'pagination'=>$pagination
            
        ]);
         
    }//ชุดแต่งงาน เลือกชุดหน้าแรก
    
    public function actionOrder($id){
        $order = new \common\models\Orderpromotion();
        $wedding = \common\models\Weddings::findOne($id);
        $post = \yii::$app->request->post();
        $image = \common\models\Uploads::find()->where(['ref'=>$wedding->ref])->one();
        if(!empty($post))
        {
            
            $count =$post["Orderpromotion"]["count"];
            if($count >  $wedding->we_count){
                \common\lib\message\Messagebox::getWarning("จำนวนชุดเหลือไม่เพียงพอ");
                return $this->render('order_form',[
                    'order'=>$order,
                    'img'=>$image,
                    'wedding'=>$wedding
                ]);
            }
            $price = $wedding->we_price*$count;
            $order->count = $count;
            $order->user_id = Yii::$app->user->identity->id;
            $order->pro_id = $id;
            $order->pro_name = $wedding->we_name;
            $order->total = $price;//
            if($order->save()){
                $c=$wedding->we_count-$count;
                $sql=Yii::$app->db->createCommand("update weddings set we_count='".$c."' where id='".$id."' ")->execute();

                $report=new \common\models\Reports();
                $report->getReport($id, $wedding->we_name, $price, Yii::$app->user->identity->id, $count, Date('Y-m-d'));
            }
            \common\lib\message\Messagebox::getSuccess("บันทึกรายการแล้ว");
            return $this->redirect(['cart/index']);
            
            
            
            
        }
        return $this->render('order_form',[
            'order'=>$order,
            'img'=>$image,
            'wedding'=>$wedding
        ]);
    }//เพิ่มการเลือกสินค้า โปรโมชั่น
}
