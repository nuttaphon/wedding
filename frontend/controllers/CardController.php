<?php
 
namespace frontend\controllers;
use yii;

use common\models\User;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;        // เรียกใช้ คลาส AccessControl
use common\components\AccessRule;
use yii\helpers\Url;    
class CardController extends \yii\web\Controller{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException("ไม่ได้รับอนุญาตกรุณา Login ");
                },
                'only' => ['order'], // กำหนด action ทั้งหมดภายใน Controller นี้
                'ruleConfig' => [
                    'class' => AccessRule::className() // เรียกใช้งาน accessRule (component) ที่เราสร้างขึ้นใหม่
                ],
                'rules' => [
                    [
                        'actions' => ['order'],     // กำหนด rules ให้ actionIndex()
                        'allow' => true,
                        'roles' => [
                             User::ROLE_USER,        // อนุญาตให้ "user ที่ login" 
                            User::ROLE_ADMIN        // อนุญาตให้ "ผู้ดูแลระบบ" ใช้งานได้
                        ],
                    ],
                     
                ],
            ],
        
        ];
    }//กำหนดให้ login ก่อนถึงจะเข้า method ได้ 



   public function actionIndex(){
        //เตียมข้อมูล card แต่งงาน select * from cards 
        $query = \common\models\Cards::find();

        //กำหนดหน้าที่จะให้แสดง มี 10 แถว
        $pagination = new \yii\data\Pagination([
            'defaultPageSize'=>10,
            'totalCount'=>$query->count(),
        ]);

        //ให้ตัวแปร card รับข้อมูลจากตัวแปร query ทีเตรียมขอ้มูลไว้ มาเรียงลำดับจากมากไปหาน้อย
        //และกำหนดจำนวนแถวที่จะแสดง ที่ตั้งไว้ 10 แถว
        $card = $query->orderBy(['id'=>SORT_DESC])
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
        
        //ส่งข้อมูลออกไปหา frontend/views/cards/index มีพารามิเตอร์ส่งไป 2 ตัว คือ card และ pagination คือ ข้อมูลการ์ด และ ตัวแป่งหน้าเมื่อข้อมูลมีมากกว่า 10 แถว คลิกที menu การ์ด ดู
        return $this->render('index',[
            'card'=>$card,
            'pagination'=>$pagination
            
        ]);
         
    }//ชุดแต่งงาน เลือกชุดหน้าแรก
    
    public function actionOrder($id){
        //สร้าง object จาก class Orderpromotion เพื่อใช้งานตัวแปร หรือ attribute ที่อยู่ข้างใน เหมือน การสืบทอด class จะได้ข้อมูลเป็น Object
        $order = new \common\models\Orderpromotion();

        //ดึงข้อมูลจากตาราง card ด้วย Primary Key ที่เป็น id  || select * from where id=$id
        $card = \common\models\Cards::findOne($id);

        //การรับค่าแบบ POST หรือ $_POST['...'];
        $post = \yii::$app->request->post();

        //ดึงข้อมูลรูปภาพจากตารางที่ชื่อว่า Uploads ในฐานข้อมูล และกำหนดเงื่อนไขให้ ref = ref เอาแค่รูปเดีย one()
        $image = \common\models\Uploads::find()->where(['ref'=>$card->ref])->one();
        if(!empty($post))//ถ้ามีการส่งค่ามาแบบ $_POST
        {
            //ให้ตัวแปร count เก็บค่า จาก form ที่ส่งมา  
            $count =$post["Orderpromotion"]["count"];

            //ตรวจสอบว่า จำนวนการ์ดที่ส่งจาก form มีมากกว่าใน ฐานข้อมูลหรือไม่
            if($count >  $card->card_amount){
                \common\lib\message\Messagebox::getWarning("จำนวนการ์ดเหลือไม่เพียงพอ");
                return $this->render('order_form',[
                    'order'=>$order,
                    'img'=>$image,
                    'card'=>$card
                ]);
            }
            //ดึงราคา การ์ด ออกมาจากฐานข้อมูล 
            $price = $card->price*$count;

            //กำหนดให้ attribute หรือ ตัวแปร ที่เรา สืบทอดมาจาก class Orderpromotion
            //วิธีการเรียกใช้ คือ $order -> count  
            //Yii::$app->user->identity->id; รหัส คนที่ login 
            $order->count = $count;
            $order->user_id = Yii::$app->user->identity->id;
            $order->pro_id = $id;//รหัส การ์ดที่ถูกส่งมาเมื่อคลิก การ์ดที่เราต้องการซื้อ /wedding/card/order?id=5
            $order->total = $price;//จำนวนเงินทั้งหมด 
            $order->pro_name = $card->card_name;//ชื่อการ์ด 
            
            if($order->save()){ //บันทึกข้อมูล ลงในตาราง order_promotion
                $c=$card->card_amount-$count;//เอาจำนวน การ์ด มา - กำจำนวนที่เลือก
                //แก้ไขจำนวน การ์ด หรือ update ใน ตาราง cards ในฐานข้อมูล 
                $sql=Yii::$app->db->createCommand("update cards set card_amount='".$c."' where id='".$id."' ")->execute(); //execute() สั่งให้ทำงาน 
            
                $report=new \common\models\Reports();
                $report->getReport($id, $card->card_name, $price, Yii::$app->user->identity->id, $count, Date('Y-m-d'));
                
            }
            //สร้าง MessageBox แสดข้อความ
            \common\lib\message\Messagebox::getSuccess("บันทึกรายการแล้ว");
            //ไปยังหน้า ตระกร้าสินค้า 
            return $this->redirect(['cart/index']);
  
        }

        //เมื่อไม่มีการส่งค่ามา แบบ $_POST จะไปเรียก views/care/order_form.php
        return $this->render('order_form',[
            'order'=>$order,
            'img'=>$image,
            'card'=>$card
        ]);
    }//เพิ่มการเลือกสินค้า โปรโมชั่น
    
    
}
