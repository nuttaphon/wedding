<?php

namespace frontend\controllers;

use Yii;
use common\models\Makes;
 
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

 use common\models\User;
 
use yii\filters\AccessControl;        // เรียกใช้ คลาส AccessControl
use common\components\AccessRule;
use yii\helpers\Url;
class MarkController extends Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException("ไม่ได้รับอนุญาตกรุณา Login ");
                },
                'only' => ['order'], // กำหนด action ทั้งหมดภายใน Controller นี้
                'ruleConfig' => [
                    'class' => AccessRule::className() // เรียกใช้งาน accessRule (component) ที่เราสร้างขึ้นใหม่
                ],
                'rules' => [
                    [
                        'actions' => ['order'],     // กำหนด rules ให้ actionIndex()
                        'allow' => true,
                        'roles' => [
                             User::ROLE_USER,        // อนุญาตให้ "user ที่ login" 
                            User::ROLE_ADMIN        // อนุญาตให้ "ผู้ดูแลระบบ" ใช้งานได้
                        ],
                    ],
                     
                ],
            ],
        
        ];
    }//กำหนดให้ login ก่อนถึงจะเข้า method ได้ 

    public function actionOrder($id){
        $order = new \common\models\Orderpromotion();
        $marks = \common\models\Makes::findOne($id);
        $post = \yii::$app->request->post();
        if(!empty($post))
        {
            //print_r($_POST);exit();
            $count =$post["Orderpromotion"]["count"];
            $price = $marks->price*1;
            $order->count = 1;
            $order->user_id = Yii::$app->user->identity->id;
            $order->pro_id = $id;
            $order->pro_name = $marks->name;
            $order->total = $price;//
            $order->save();

            $report=new \common\models\Reports();
                $report->getReport($id, $marks->name, $price, Yii::$app->user->identity->id, 1, Date('Y-m-d'));


            \common\lib\message\Messagebox::getSuccess("บันทึกรายการแล้ว");
            return $this->redirect(['cart/index']);
        }
        return $this->render('order_form',[
            'order'=>$order,
            'marks'=>$marks,
        ]);
    }//เพิ่มการเลือกสินค้า โปรโมชั่น
    
    
    public function actionIndex()
    {
       $query = \common\models\Makes::find();
        $pagination = new \yii\data\Pagination([
            'defaultPageSize'=>10,
            'totalCount'=>$query->count(),
        ]);
        $makes = $query->orderBy(['id'=>SORT_DESC])
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
        
        
        return $this->render('index',[
            'pagination'=>$pagination,
            'makes'=>$makes
            
        ]);
    }

    
     

     
}
