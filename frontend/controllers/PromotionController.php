<?php

namespace frontend\controllers;

use Yii;
use common\models\Promotion;
use frontend\models\PromotionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\User;
 
use yii\filters\AccessControl;        // เรียกใช้ คลาส AccessControl
use common\components\AccessRule;
use yii\helpers\Url;
 
class PromotionController extends Controller
{
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException("ไม่ได้รับอนุญาตกรุณา Login ");
                },
                'only' => ['order'], // กำหนด action ทั้งหมดภายใน Controller นี้
                'ruleConfig' => [
                    'class' => AccessRule::className() // เรียกใช้งาน accessRule (component) ที่เราสร้างขึ้นใหม่
                ],
                'rules' => [
                    [
                        'actions' => ['order'],     // กำหนด rules ให้ actionIndex()
                        'allow' => true,
                        'roles' => [
                             User::ROLE_USER,        // อนุญาตให้ "user ที่ login" 
                            User::ROLE_ADMIN        // อนุญาตให้ "ผู้ดูแลระบบ" ใช้งานได้
                        ],
                    ],
                     
                ],
            ],
        
        ];
    }//กำหนดให้ login ก่อนถึงจะเข้า method ได้ 

    public function actionOrder($id){
        $order = new \common\models\Orderpromotion();
        $promotion = Promotion::findOne($id);
        $post = \yii::$app->request->post();
        if(!empty($post))
        {
            //print_r($_POST);exit();
            $count =$post["Orderpromotion"]["count"];
            $price = $promotion->price*$count;
            $order->count = $count;
            $order->user_id = Yii::$app->user->identity->id;
            $order->pro_id = $id;
             $order->pro_name = $promotion->pro_name;
            $order->total = $price;//
            $order->save();


            $report=new \common\models\Reports();
                $report->getReport($id, $promotion->pro_name, $price, Yii::$app->user->identity->id, $count, Date('Y-m-d'));

                
            \common\lib\message\Messagebox::getSuccess("บันทึกรายการแล้ว");
            return $this->redirect(['cart/index']);
        }
        return $this->render('order_form',[
            'order'=>$order,
            'promotion'=>$promotion,
        ]);
    }//เพิ่มการเลือกสินค้า โปรโมชั่น
    public function actionOrderupdate($id){
        $order = \common\models\Orderpromotion::findOne($id);
        $promotion = Promotion::findOne($id);
        $post = \yii::$app->request->post();
        if(!empty($post))
        {
            //print_r($_POST);exit();
            $count =$post["Orderpromotion"]["count"];
            $price = $promotion->price*$count;
            $order->count = $count;
            $order->user_id = Yii::$app->user->identity->id;
            $order->pro_id = $id;
            $order->total = $price;//
            $order->save();

             $report=new \common\models\Reports();
                $report->getReport($id, $promotion->pro_name, $price, Yii::$app->user->identity->id, $count, Date('Y-m-d'));

            \common\lib\message\Messagebox::getSuccess("บันทึกรายการแล้ว");
            return $this->redirect(['cart/index']);
        }
        return $this->render('order_form',[
            'order'=>$order,
            'promotion'=>$promotion,
        ]);
    }//แก้ไข การเลือกสินค้า โปรโมชั่น
    
    
    
    public function actionIndex()
    {
       $query = \common\models\Promotion::find();
        $pagination = new \yii\data\Pagination([
            'defaultPageSize'=>10,
            'totalCount'=>$query->count(),
        ]);
        $promotion = $query->orderBy(['id'=>SORT_DESC])
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
        
        
        return $this->render('index',[
            'pagination'=>$pagination,
            'promotion'=>$promotion
            
        ]);
    }

    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Promotion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Promotion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Promotion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Promotion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Promotion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Promotion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Promotion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
