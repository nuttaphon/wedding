<?php

 

namespace frontend\controllers;
use Yii;
class CartController extends \yii\web\Controller{
    public function actionIndex(){
        $user_id = Yii::$app->user->identity->id;
        $order = \common\models\Orderpromotion::find()->where(['user_id'=>$user_id])->all();
        $sum = Yii::$app->db->createCommand("select sum(total) as s from order_promotion where user_id ='". $user_id."' ")->queryOne();
        $sums= $sum['s'];
    
        
        return $this->render('index',[
            'model'=>$order,
            'sums'=>$sums
        ]);
    }//ตระกร้าสินค้าหน้าาแรก
    public function actionUpdate($id){
        
    }
    
    public function actionDelete($id){
        $model = \common\models\Orderpromotion::findOne($id);
        if($model->delete()){
            \common\lib\message\Messagebox::getSuccess('ลบข้อมูลแล้ว');
            return $this->redirect(['index']);
        }
    }

    public function actionConfirms($id){
        $sql="select * from order_promotion where user_id='".$id."' ";
        $query=Yii::$app->db->createCommand($sql)->queryAll();
       
        foreach($query as $q){
             $model= new \common\models\Orders();
             $model->pro_id = $q['pro_id'];
             $model->pro_name = $q['pro_name'];
             $model->user_id = $q['user_id'];
             $model->count = $q['count'];
             $model->total = $q['total'];
             $model->sitecode= Yii::$app->user->identity->password_hash;
             $model->save();
        }
        
         $sql="delete from order_promotion where user_id='".$id."' ";
        $query=Yii::$app->db->createCommand($sql)->execute();
        \common\lib\message\Messagebox::getSuccess("ส่งข้อมูลเรียบร้อย");
        return $this->redirect(['index']);
//        return $this->render('print',[
//            'user_id'=>Yii::$app->user->identity->id
//        ]);
    }
}
