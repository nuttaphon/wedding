-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2017 at 12:57 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `love`
--

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(11) NOT NULL,
  `card_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_amount` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_st` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_st` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `card_name`, `card_amount`, `price`, `created_st`, `updated_st`, `ref`) VALUES
(4, 'การ์ด1', 100, 20, '2017-02-15 16:42:28', '2017-02-15 16:42:28', 'Va38OJMsmkTrqS_Fsz0NsY'),
(5, 'การ์ด2', 997, 21, '2017-02-15 16:52:29', '2017-02-15 16:52:29', 'NpmBNUaWneTofy0hKEQ3R_');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`) VALUES
(1, '<p><strong>เบอร์โทร 0823043416</strong></p>\r\n\r\n<p><strong>อีเมล์ admin@gmail.com</strong></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` int(11) NOT NULL,
  `ma_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ma_description` text COLLATE utf8_unicode_ci NOT NULL,
  `ma_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`id`, `ma_name`, `ma_description`, `ma_price`, `ref`) VALUES
(2, 'พิธีแต่งงานแบบล้านนา', 'พิธีแต่งงานแบบล้านนา', '45000', 'voZFGNqedACqQwtyMoLpKu');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1486902240),
('m130524_201442_init', 1486902243);

-- --------------------------------------------------------

--
-- Table structure for table `order_promotion`
--

CREATE TABLE `order_promotion` (
  `id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `pro_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_promotion`
--

INSERT INTO `order_promotion` (`id`, `pro_id`, `pro_name`, `user_id`, `count`, `total`) VALUES
(18, 5, 'การ์ด2', 6, 1, 21);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `name`) VALUES
(1, '<p>ทดสอบการเพิ่มวิธีการชำระเงิน</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `fname`, `lname`) VALUES
(1, 'Chanpan007', 'nuttaphon'),
(2, 'tanus', 'vijitpunya'),
(3, 'kaitisung', 'khonnang');

-- --------------------------------------------------------

--
-- Table structure for table `photo_l`
--

CREATE TABLE `photo_l` (
  `id` int(11) NOT NULL,
  `ref` varchar(50) DEFAULT NULL COMMENT 'เลข fk กับ upload ใช้กับ upload ajax',
  `event_name` varchar(255) DEFAULT NULL COMMENT 'ชื่องาน',
  `detail` text COMMENT 'รายละเอียด',
  `start_date` datetime DEFAULT NULL COMMENT 'วันที่เริ่มถ่ายภาพ',
  `end_date` datetime DEFAULT NULL COMMENT 'วันที่ถ่ายภาพเสร็จ',
  `customer_name` varchar(150) DEFAULT NULL COMMENT 'ชื่อลูกค้า',
  `customer_mobile_phone` varchar(20) DEFAULT NULL COMMENT 'เบอร์โทร'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `photo_l`
--

INSERT INTO `photo_l` (`id`, `ref`, `event_name`, `detail`, `start_date`, `end_date`, `customer_name`, `customer_mobile_phone`) VALUES
(1, 'WD8gGH-LpBkFSGBBPKHT3W', 'ทดสอบ', 'ทดสอบ', '2017-02-15 00:00:00', NULL, NULL, NULL),
(2, '87sfHPeG3SXpdJ9XMCvURI', 'ทดสอบ2', 'ทดสอบ2', '2017-02-15 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `pro_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `price` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `pro_name`, `image`, `date`, `price`) VALUES
(4, 'test', '98rN6YFeMTXiuH9sKZoN6VIhTBKCObbj.jpg', '2017-02-14 16:38:30', 9900),
(5, 'test', '227STP-WvrqzH_45LjuFc-3UTzGcOBqV.jpg', '2017-02-15 14:58:50', 19999),
(6, 'test', 'v0VbcDDObiAUwKgTZ0yKYGecAS2fUegT.jpg', '2017-02-15 14:59:11', 6500),
(7, 'test', 'wTpxj1TjhhdixJEit0ytIUPBsyC8cQrf.jpg', '2017-02-15 15:00:09', 9900);

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `upload_id` int(11) NOT NULL,
  `ref` varchar(50) DEFAULT NULL,
  `file_name` varchar(150) DEFAULT NULL COMMENT 'ชื่อไฟล์',
  `real_filename` varchar(150) DEFAULT NULL COMMENT 'ชื่อไฟล์จริง',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `type` int(11) DEFAULT NULL COMMENT 'ประเภท'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`upload_id`, `ref`, `file_name`, `real_filename`, `create_date`, `type`) VALUES
(29, 'prvfIYHHBzxONk5kNocV5s', 'card3.jpg', '73e2a02a685aa68a68e3b387a24953a0.jpg', '2017-02-14 15:26:02', NULL),
(33, 'JfS1DklEjJ3Nqf-y-G4TY4', 'card1.jpg', 'cc9cec9920dc4c96b999542c8b58fa94.jpg', '2017-02-14 15:42:54', NULL),
(34, 'BM06ZMqudqoBf-iGk6sCpe', 'wdding1.jpg', '2eeed270eea7a0a4259a870941eec88a.jpg', '2017-02-14 15:43:27', NULL),
(38, 'QcfdUXTlrwbqSRvRieXARJ', 'fw1.jpg', '70cae69716d3331ff2b04792e74ed9db.jpg', '2017-02-15 02:20:09', NULL),
(39, 'QcfdUXTlrwbqSRvRieXARJ', 'fw3.jpg', '57d84de3c2cf6043904433d77daabf2c.jpg', '2017-02-15 02:20:09', NULL),
(40, 'QcfdUXTlrwbqSRvRieXARJ', 'fw4.jpg', 'd0bb24e891ab393a8383ca8844391471.jpg', '2017-02-15 02:20:09', NULL),
(41, 'QcfdUXTlrwbqSRvRieXARJ', 'fw2.jpg', '7acab195aa5becc5b3e857cda5232345.jpg', '2017-02-15 02:20:09', NULL),
(42, 'WD8gGH-LpBkFSGBBPKHT3W', 'format3.jpg', '49f195fd6bbda3b8a0d2aa99034ee616.jpg', '2017-02-15 13:54:22', NULL),
(43, 'WD8gGH-LpBkFSGBBPKHT3W', 'format2.jpg', '2730622c16fd627f6b0f4a09ab4267bf.jpg', '2017-02-15 13:54:22', NULL),
(44, 'WD8gGH-LpBkFSGBBPKHT3W', 'format1.jpg', '59a5561bbbeb9f88c5ed5f7454feb33d.jpg', '2017-02-15 13:54:22', NULL),
(45, '87sfHPeG3SXpdJ9XMCvURI', 'format2.jpg', 'fcd1ff8bbd9f60cdba4e85678487f5bb.jpg', '2017-02-15 14:29:57', NULL),
(46, 'Mkal-Sy8UcUWq8uQ9E_BWF', 'wdding1.jpg', 'fe3ee28700de6f4f2cbeeaad190c249c.jpg', '2017-02-15 15:50:19', NULL),
(47, '6cIucW2EELEdFFUIgh4uAf', 'wdding2.jpg', '7a23dd128c52bec32695e1186e4194a7.jpg', '2017-02-15 15:50:40', NULL),
(48, 'WeN-yHn-ApDDltv2AI6FZk', 'wedding3.jpg', '7d521be757c18abdfbea1376a20457ea.jpg', '2017-02-15 15:50:59', NULL),
(49, 'Va38OJMsmkTrqS_Fsz0NsY', 'card1.jpg', '97c54280b5a913291ec392543fe0a8f9.jpg', '2017-02-15 16:42:26', NULL),
(50, 'NpmBNUaWneTofy0hKEQ3R_', 'card2.jpg', '77a806736dfda9998c9fab2db3872c82.jpg', '2017-02-15 16:52:28', NULL),
(51, 'voZFGNqedACqQwtyMoLpKu', 'format1.jpg', 'de888dd3c96478a8e0674832e2b84462.jpg', '2017-02-15 17:07:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `fname`, `lname`, `phone`, `status`, `created_at`, `updated_at`, `role`) VALUES
(1, 'admin', '', 'nRIv5Xj-jA7QtEceRGsFjVLQdMXdwUxJ', '$2y$13$djOKyy7G/FaAcqQqB1JwQ.HYFkIfHluJyFBznvNRz6zo0G5TMYSI6', NULL, 'admin@gmail.com', 'admins', 'admin', '0862229417', 10, 1486902584, 1486912608, 'USER'),
(2, 'webmaster', '$2y$13$KbdJwQHjjj/5K4FRIEdw9uRMAGycQ8hnbDP7K0jiSm6jllRRXAw4O', 'fOwDAUgVPV_T4y3EnNlHO0Y9vJcbSHg8', '$2y$13$KbdJwQHjjj/5K4FRIEdw9uRMAGycQ8hnbDP7K0jiSm6jllRRXAw4O', NULL, 'webmaster@gmail.com', 'webmaster', 'webmaster', '0862229416', 10, 1486902987, 1486902987, 'ADMIN'),
(6, 'member', '', '', '$2y$13$NNe3ZmwdLTbflMO2qqL4GOuoBxidIz83pzRwKFvmwyAHFMaBcaFbG', NULL, 'member@gmail.com', 'member', 'member', '3434343434', 10, 0, 0, 'USER');

-- --------------------------------------------------------

--
-- Table structure for table `weddings`
--

CREATE TABLE `weddings` (
  `id` int(11) NOT NULL,
  `we_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `we_description` text COLLATE utf8_unicode_ci NOT NULL,
  `we_count` int(11) NOT NULL,
  `we_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `weddings`
--

INSERT INTO `weddings` (`id`, `we_name`, `we_description`, `we_count`, `we_price`, `ref`) VALUES
(2, 'wedding1', 'wedding1', 10, '5000', 'Mkal-Sy8UcUWq8uQ9E_BWF'),
(3, 'wedding2', 'wedding2', 5, '6000', '6cIucW2EELEdFFUIgh4uAf'),
(4, 'wedding3', 'wedding3', 10, '6500', 'WeN-yHn-ApDDltv2AI6FZk');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `order_promotion`
--
ALTER TABLE `order_promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo_l`
--
ALTER TABLE `photo_l`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref` (`ref`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `weddings`
--
ALTER TABLE `weddings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_promotion`
--
ALTER TABLE `order_promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `photo_l`
--
ALTER TABLE `photo_l`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `weddings`
--
ALTER TABLE `weddings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
